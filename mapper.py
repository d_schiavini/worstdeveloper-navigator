import asyncio
from pyppeteer import launch

class Mapper:

    def __init__(self):
        print("constructor")
        self.mapper={
        "scrollup":self.scrollUp(),
        "scrolldown":self.scrollDown(),
        "admin":self.admin(),
        "allstats":self.allStats(),
        "stats":self.stats()
        }

    async def initializer(self):
        print("initializer")
        self.browser = await launch({"headless":0,#"executablePath":"C:/Users/Damiano/AppData/Local/Chromium/Application/chrome.exe",
        "args":["--start-fullscreen", "--allow-cross-origin-auth-prompt"]})
        self.page = await self.browser.newPage()
        await self.page.setViewport({"width":1920,"height":1080})

    async def map(self,section):
        section=section.lower()
        if section in self.mapper.keys():
            await self.mapper[section]
            
    async def admin(self):
        await self.page.goto('http://localhost:3001/admin')

    async def allStats(self):
        await self.page.goto('http://localhost:3001/all')

    async def stats(self):
        await self.page.goto('http://localhost:3001/')

    async def scrollUp(self):
        await self.page.evaluate("window.scrollBy(0, document.body.scrollHeight*-1)")

    async def scrollDown(self):
        await self.page.evaluate("window.scrollBy(0, document.body.scrollHeight)")

    async def close(self):
        await self.browser.disconnect()
        await self.browser.close()


    