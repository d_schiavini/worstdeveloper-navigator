from mapper import Mapper
import asyncio


async def main():
    mapper = Mapper()
    await mapper.initializer()
    inp = input("inserisci")
    await mapper.map(inp)
    inp = input("inserisci")
    await mapper.map(inp)
    inp = input("inserisci")
    await mapper.map(inp)
    inp = input("inserisci")
    await mapper.map(inp)
    inp = input("inserisci")
    await mapper.map(inp)
    mapper.close

asyncio.run(main())
